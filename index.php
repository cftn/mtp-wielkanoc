<?php $home = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]"; ?>
<?php
  function translate($string, $stringEn) {
    if(empty($_GET['lang'])) {
      echo $string;
    } else {
      echo $stringEn;
    }
  }
?>
<!doctype html>

<html lang="en" xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">
<head>
  <meta charset="utf-8">

  <title><?php translate('Wyślij kartkę wielkanocną z twoimi życzeniami', 'Send Easter card with your wishes') ?></title>
  <meta name="description" content="MTP - kartka wielkanocna">
  <meta name="author" content="Crafton">
	<meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="images/favicons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="images/favicons/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="images/favicons/manifest.json">
  <link rel="mask-icon" href="images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">
	<link rel="icon" href="images/favicons/favicon.ico" type="image/x-icon"/>

  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <!-- <link href="https://fonts.googleapis.com/css?family=Shadows+Into+Light+Two&amp;subset=latin-ext" rel="stylesheet"> -->
  <link rel="preload" href="fonts/shadow/shadowsintolighttwo-regular-webfont.woff2" as="font" type="font/woff2">
  <link rel="stylesheet" href="stylesheets/style.css">

	<meta property="og:url" content="<?php echo $home ?>">
	<meta property="og:image" content="" />
	<meta property="og:image:width" content="1101" />
	<meta property="og:image:height" content="772" />
	<meta property="og:title" content="MTP - kartka wielkanocna"/>
	<meta property="og:site_name" content="MTP - kartka wielkanocna"/>
	<meta property="og:description" content="MTP - kartka wielkanocna"/>

  <!--[if lt IE 9]>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.js"></script>
  <![endif]-->
</head>

<body>
  <span class="preload" style="font-family:Shadows Into Light Two;"></span>
  <main>
    <div class="wrapper">
			<div class="header-mobile">
				<a href="http://www.mtp.pl" target="_blank">
					<img src="images/logo.png" alt="MTP logo">
				</a>
				<h1><?php translate('Wyślij kartkę wielkanocną <br> z twoimi życzeniami', 'Send Easter card <br> with your wishes') ?></h1>
			</div>
      <div class="canvas-container">
        <canvas id="ugly-sweter" width="739" height="772">
          Twoja przeglądarka nie wspiera canvas :/
        </canvas>
      </div>
      <div class="generator">
        <div class="container">
					<div class="header">
						<a href="http://www.mtp.pl" target="_blank">
							<img src="images/logo.png" alt="MTP logo">
						</a>
						<h1><?php translate('Wyślij kartkę wielkanocną <br> z twoimi życzeniami', 'Send Easter card <br> with your wishes') ?></h1>
					</div>
          <div class="inputs">
            <h4><?php translate('Wpisz treść życzeń:', 'Write your wishes') ?></h4>
							<input class="input" id="jsTop" name="first" type="text" data-line="firstLine" value="<?php translate('Wesołych Świąt', 'Happy Easter') ?>">
	            <input class="input" id="jsMiddle" name="second" type="text"  data-line="secondLine" value="<?php translate('Życzą', 'From') ?>">
	            <input class="input" id="jsBottom" name="third" type="text" name="name" data-line="thirdLine" value="<?php translate('Targi Poznańskie', 'Poznań International Fair') ?>">
          </div>
          <div class="choose-color">
            <h3><?php translate('Wybierz stylizację: ', 'Choose your style: ') ?></h3>
            <div class="color-container">
              <div class="color red" data-attribute="red"></div>
              <div class="color yellow" data-attribute="yellow"></div>
              <div class="color blue" data-attribute="blue"></div>
            </div>
          </div>
          <a class="generate-button" id="generate" type="button" name="button"><?php translate('Generuj kartkę', 'Generate card') ?></a>
          <a class="lang" href="<?php if( empty($_GET['lang']) ) { echo "http://" . $_SERVER['SERVER_NAME'] . $_SERVER['REQUEST_URI'] . '?lang=en'; } else { echo "http://" . $_SERVER['SERVER_NAME'] . str_replace('?lang=en','',$_SERVER['REQUEST_URI']); } ?>"  type="button" name="button"><?php translate('EN', 'PL') ?></a>
        </div>
      </div>
    </div>
  </main>
	<div class="modal">
		<div class="social">
			<h1><?php translate('Jaki stylowy zając!', 'What a beautiful bunny!') ?></h1>
			<h2><?php translate('Twoja kartka jest gotowa do wysyłki.', 'Your card is ready to send.') ?></h2>
			<p><?php translate('Bezpośredni link do kartki: ', 'Direct link: ') ?><a class="direct" target="_blank" href=""></a></p>
			<h3><?php translate('Udostępnij swoją kartkę: ', 'Share your card: ') ?></h3>
			<div class="social-container">

				<a class="fb" target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=<?php echo $home ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="20" viewBox="0 0 24 24"><path d="M9 8h-3v4h3v12h5v-12h3.642l.358-4h-4v-1.667c0-.955.192-1.333 1.115-1.333h2.885v-5h-3.808c-3.596 0-5.192 1.583-5.192 4.615v3.385z"/></svg>
				</a>
				<a class="twitter" target="_blank" href="https://twitter.com/home?status=<?php echo $home ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M24 4.557c-.883.392-1.832.656-2.828.775 1.017-.609 1.798-1.574 2.165-2.724-.951.564-2.005.974-3.127 1.195-.897-.957-2.178-1.555-3.594-1.555-3.179 0-5.515 2.966-4.797 6.045-4.091-.205-7.719-2.165-10.148-5.144-1.29 2.213-.669 5.108 1.523 6.574-.806-.026-1.566-.247-2.229-.616-.054 2.281 1.581 4.415 3.949 4.89-.693.188-1.452.232-2.224.084.626 1.956 2.444 3.379 4.6 3.419-2.07 1.623-4.678 2.348-7.29 2.04 2.179 1.397 4.768 2.212 7.548 2.212 9.142 0 14.307-7.721 13.995-14.646.962-.695 1.797-1.562 2.457-2.549z"/></svg>
				</a>
				<a class="instagram" target="_blank" href="https://pinterest.com/pin/create/button/?url=<?php echo $home ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M12 0c-6.627 0-12 5.372-12 12 0 5.084 3.163 9.426 7.627 11.174-.105-.949-.2-2.405.042-3.441.218-.937 1.407-5.965 1.407-5.965s-.359-.719-.359-1.782c0-1.668.967-2.914 2.171-2.914 1.023 0 1.518.769 1.518 1.69 0 1.029-.655 2.568-.994 3.995-.283 1.194.599 2.169 1.777 2.169 2.133 0 3.772-2.249 3.772-5.495 0-2.873-2.064-4.882-5.012-4.882-3.414 0-5.418 2.561-5.418 5.207 0 1.031.397 2.138.893 2.738.098.119.112.224.083.345l-.333 1.36c-.053.22-.174.267-.402.161-1.499-.698-2.436-2.889-2.436-4.649 0-3.785 2.75-7.262 7.929-7.262 4.163 0 7.398 2.967 7.398 6.931 0 4.136-2.607 7.464-6.227 7.464-1.216 0-2.359-.631-2.75-1.378l-.748 2.853c-.271 1.043-1.002 2.35-1.492 3.146 1.124.347 2.317.535 3.554.535 6.627 0 12-5.373 12-12 0-6.628-5.373-12-12-12z" fill-rule="evenodd" clip-rule="evenodd"/></svg>
				</a>
				<a class="mail" href="mailto:?&body=<?php echo $home ?>">
					<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M0 3v18h24v-18h-24zm6.623 7.929l-4.623 5.712v-9.458l4.623 3.746zm-4.141-5.929h19.035l-9.517 7.713-9.518-7.713zm5.694 7.188l3.824 3.099 3.83-3.104 5.612 6.817h-18.779l5.513-6.812zm9.208-1.264l4.616-3.741v9.348l-4.616-5.607z"/></svg>
				</a>
			</div>
			<a class="close">
				<svg xmlns="http://www.w3.org/2000/svg" width="18" height="18" viewBox="0 0 24 24"><path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/></svg>
			</a>
		</div>
	</div>
  <script src="javascripts/script.js"></script>
</body>
</html>
