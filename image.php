<?php
	$link = "http://$_SERVER[HTTP_HOST]" . '/' . $_GET['img']; //Dodaj właściwą ściezke katalogu
	$home = "http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
  function translate($string, $stringEn) {
    if(empty($_GET['lang'])) {
      echo $string;
    } else {
      echo $stringEn;
    }
  }
?>
<!doctype html>

<html xmlns="http://www.w3.org/1999/xhtml"
      xmlns:fb="http://ogp.me/ns/fb#">
<head>
	<meta charset="utf-8">

  <title><?php translate('Wyślij kartkę wielkanocną z twoimi życzeniami', 'Send Easter card with your wishes') ?></title>
  <meta name="description" content="MTP - kartka wielkanocna">
  <meta name="author" content="Crafton">
	<meta name="viewport" content="width=device-width, initial-scale=1">

  <link rel="apple-touch-icon" sizes="120x120" href="images/favicons/apple-touch-icon.png">
  <link rel="icon" type="image/png" href="images/favicons/favicon-32x32.png" sizes="32x32">
  <link rel="icon" type="image/png" href="images/favicons/favicon-16x16.png" sizes="16x16">
  <link rel="manifest" href="images/favicons/manifest.json">
  <link rel="mask-icon" href="images/favicons/safari-pinned-tab.svg" color="#5bbad5">
  <meta name="theme-color" content="#ffffff">
	<link rel="icon" href="images/favicons/favicon.ico" type="image/x-icon"/>

  <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
  <link rel="stylesheet" href="stylesheets/style.css">

	<meta property="og:image" content="<?php echo $link ?>" />
	<meta property="og:image:width" content="738" />
	<meta property="og:image:height" content="733" />
	<meta property="og:type" content="website" />
	<meta property="og:url" content="<?php echo $home ?>" />
	<meta property="og:title" content="MTP - kartka wielkanocna"/>
	<meta property="og:site_name" content="MTP - kartka wielkanocna"/>
	<meta property="og:description" content="MTP - kartka wielkanocna"/>
	<meta property="fb:app_id" content="966242223397117" />
</head>
<body>
	<main class="output">
		<div class="canvas-container output">
				<img src="<?php echo $link ?>" alt="kartka wielkanocna">
		</div>
		<a class="generate-button bottom" href="http://wielkanoc.mtp.pl"><?php if( empty($_GET['lang']) ) { echo 'Wygeneruj swoją kartkę'; } else { echo 'Generate a card'; } ?></a>
	</main>
</body>
<html>
