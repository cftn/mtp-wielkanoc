var core = function() { this.init(); };
core.prototype = {
   init: function() {
      this._run();
   },

   _run: function() {
      document.alphabet = new alphabet();
      document.changeBackground = new changeBackground();
			document.download = new download();
			document.removeValue = new removeValue();
   }
};

$(document).ready(function() {
   new core();
});
