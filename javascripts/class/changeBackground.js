var changeBackground = function() { this.init(); };
changeBackground.prototype = {
	 _currBackground: null,
	 _currentColor: 'blue',
   _imgs: [],
   _ready: false,
   init: function () { var $this = this;
      if(!this._setVars()) return;
      this._setEvents();
      this._preloadImg();
	 },

   _setVars: function() { var $this = this;
     this._canvas = document.getElementById('ugly-sweter');
		 if(!this._canvas) return;
		 this._context = document.getElementById('ugly-sweter').getContext("2d");

		 this._colorContainer = $('.color-container');
     if(!this._colorContainer) return;

     this._red = this._colorContainer.find('.red');
     this._white = this._colorContainer.find('.white');
     this._blue = this._colorContainer.find('.blue');
		 this._color = this._colorContainer.find('.color');

		 this._currBackground = new Image();
		 this._currBackground.src = "images/blue.png"; // Set background on site load
     return true;
   },

   _setEvents: function() { var $this = this;
		 $('body').on('draw', function() {
      if( !$this._ready) return;
			$this._context.clearRect(0,0, $this._canvas.width, $this._canvas.height);
      $this._context.globalAlpha = document.alphabet.opacity;
      $this._context.drawImage($this._currBackground,0,0);

      document.alphabet.pushText();
		 });
		 this._color.click(function() {
			 var src = "images/"+ $(this).attr("data-attribute") + ".png";
			 var color = $(this).attr("data-attribute");
			 $this._currentColor = color;
			 $this._currBackground.src = src;
       document.alphabet.animateVal();
			 $('body').trigger('draw');
		 });
   },
   _preloadImg: function() {
		var $this = this;
    var imgs = $('.color');
		for(var i = 0; i < imgs.length; i++) {
			var src = "images/" + imgs[i].getAttribute('data-attribute') + ".png";
			var img = new Image();
			img.onload = function() {
				$this._imgs.push(this);
				$this._checkReady();
			};
			img.src = src;
		}
	},
	_checkReady: function() {
		if ($('.color').length == this._imgs.length && this._img !== null) {
			this._ready = true;
      $('body').trigger('draw');
		}
	},
};
