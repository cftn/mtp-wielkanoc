var alphabet = function() { this.init(); };
alphabet.prototype = {
	 _settings: {
     offsetX: 220,
     offsetY: 85,
     clipW: 330,
     clipH: 160,
     font: 'Shadows Into Light Two',
     size: '33px',
     rotate: '4',
     color: '#1c3464',
   },
   _text: {
     0: [null, null], // TEXT AND STATUS
     1: [null, null],
     2: [null, null]
   },
   opacity: 1,
   init: function () { var $this = this;
      if(!this._setVars()) return;
      this._setEvents();
			this._setInitialVal();
   },

   _setVars: function() { var $this = this;
		 this._canvas = document.getElementById('ugly-sweter');
		 this._ctx = this._canvas.getContext("2d");
		 this._containerWidth = this._canvas.width;

		 this._input = $('.input');
		 this._inputAttr = this._input.attr('data-line');

		 this._topInput = $('#jsTop');
		 this._middleInput = $('#jsMiddle');
		 this._bottomInput = $('#jsBottom');

		 return true;
	 },

   _setEvents: function() { var $this = this;
     $('.input').on('propertychange input', function() {

       var textCont = [];
       var text1 = $this._topInput.val();
       var text2 = $this._middleInput.val();
       var text3 = $this._bottomInput.val();
       textCont.push(text1, text2, text3);

       var ctx = $this._ctx;
       var settings = ''+ $this._settings.size +' '+ $this._settings.font +'';
       var color = $this._settings.color;
       ctx.save();
       ctx.rotate(-$this._settings.rotate*Math.PI/180);
       ctx.shadowColor = "white";
       ctx.shadowOffsetX = 0.5;
       ctx.shadowOffsetY = 0.5;
       ctx.font = settings;
       ctx.fillStyle = color;
       ctx.textAlign = "center";

       for(var i=0; i<textCont.length; i++) {
         if( ctx.measureText(textCont[i]).width <= $this._settings.clipW - 20 ) { // Check if text fits to canvas
           if(i === 0) {
             $this._text[i][0] = $this._topInput.val();
           } else if ( i=== 1) {
             $this._text[i][0] = $this._middleInput.val();
           } else if ( i=== 2) {
             $this._text[i][0] = $this._bottomInput.val();
           }
         } else {
           if(i === 0) {
             $this._topInput.val( $this._topInput.val().slice(0, -1) );
           } else if ( i=== 1) {
             $this._middleInput.val( $this._middleInput.val().slice(0, -1));
           } else if ( i=== 2) {
             $this._bottomInput.val( $this._bottomInput.val().slice(0, - 1));
           }
         }
       }


       ctx.restore();

       $('body').trigger('draw');
     });
   },
   _setInitialVal: function() {
     this._text[0][0] = this._topInput.val();
     this._text[1][0] = this._middleInput.val();
     this._text[2][0] = this._bottomInput.val();
   },
	 pushText: function(e) { var $this = this;

     var settings = ''+ this._settings.size +' '+ this._settings.font +'';
     var color = this._settings.color;
     var ctx = this._ctx;
     var set = this._settings;

     ctx.save();
     ctx.rotate(-this._settings.rotate*Math.PI/180);
     ctx.shadowColor = "white";
     ctx.shadowOffsetX = 0.5;
     ctx.shadowOffsetY = 0.5;
     ctx.font = settings;
     ctx.fillStyle = color;
     ctx.textAlign = "center";

     var offset = 0;

     for(var y = 0; y<Object.keys(this._text).length; y++ ) {
       var singleText = this._text[Object.keys(this._text)[y]];
       singleText[1] = false;

       if(ctx.measureText(singleText[0]).width <= this._settings.clipW - 20) {
         singleText[1] = true;   // Set status
       }

       if(singleText[1]) {  // Check status
         var singleTextToDraw = this._text[Object.keys(this._text)[y]];
         ctx.fillText(singleTextToDraw[0],  set.offsetX + set.clipW / 2 , set.offsetY + 33 + offset);
         offset += 50;
       }
     }

     ctx.restore();

	 },
   animateVal: function() {
     var $this = this;
     $({trans: 0}).animate({trans: 1}, {
		     duration: 500,
  		step: function() {
  			$this.opacity = this.trans;
        $('body').trigger('draw');
  		},
  		complete: function() {
  			$this.opacity = 1;
  		}
		});
   }
};
