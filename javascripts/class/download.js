var download = function() { this.init(); };
download.prototype = {
   init: function () { var $this = this;
      if(!this._setVars()) return;
      this._setEvents();
   },

   _setVars: function() { var $this = this;
		 this._canvas = document.getElementById('ugly-sweter');
		 this._button = $('.generate-button');
		 this._modal = $('.modal');
		 this._socialContainer = $('.social');
		 this._container = $('.social-container');
		 this._socialLinks = this._container.find('a');
		 this._link = this._socialContainer.find('.direct');
		 this._closeModal = $('.close');
		 this._head = $('head');
		 this._input =  $('.input');

     return true;
   },

   _setEvents: function() { var $this = this;
			this._closeModal.click(function() {
				$this._modal.removeClass('show');
			});
			this._button.click(function() {
				$this._download();
			});
   },
   _download: function() { var $this = this;
		$.ajax({
		  type: "POST",
		  url: "upload.php",
		  data: {
		     imgBase64: $this._canvas.toDataURL()
		  }
		}).done(function(o) {
		  $this._modal.addClass('show');
			$this._link.html(o.replace('uploads/photo', ''));
			$this._link.attr('href', o);
			$this._socialLinks.each(function() {
        var href;
        if(this.href.indexOf('?lang=en') != -1) {
          this.href = this.href.replace('?lang=en', '');
          href = 'image.php?'+'img=' + o + "&" +'title=' + $this._input.val() + '&lang=en';
        } else {
          href = 'image.php?'+'img=' + o + "&" +'title=' + $this._input.val() ;
        }
				this.href += href;
			});
		});
	 }
};
