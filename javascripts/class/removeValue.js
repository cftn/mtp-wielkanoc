var removeValue = function() { this.init(); };
removeValue.prototype = {
   init: function () { var $this = this;
      if(!this._setVars()) return;
      this._setEvents();
   },

   _setVars: function() { var $this = this;
		 this._inputs = $('.inputs');
		 this._input = this._inputs.find('input');

     return true;
   },

   _setEvents: function() { var $this = this;
		 this._input.click(function() {
			 var attr = $(this).attr('clicked');
			 if(typeof attr !== typeof undefined && attr !== false) {
				 
			 } else {
				 $(this)[0].setSelectionRange(0, this.value.length);
				 $(this).attr('clicked', 'true');
			 }
		 });
	 }
};
