'use strict';

// Load plugins
var gulp = require('gulp');
var sass = require('gulp-sass');
var jshint = require('gulp-jshint');
var stripdebug = require('gulp-strip-debug');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var notify = require('gulp-notify');
var plumber = require('gulp-plumber');
var gutil = require('gulp-util');
var browsersync = require('browser-sync');

// error function for plumber
var onError = function (err) {
  gutil.beep();
  console.log(err);
  this.emit('end');
};

// BrowserSync proxy
gulp.task('browser-sync', function() {
	browsersync.init({
		proxy: 'localhost/mtp-wielkanoc/',
		port: 3000
	});
});

// BrowserSync reload all Browsers
gulp.task('browsersync-reload', function () {
	browsersync.reload();
});

// CSS task
gulp.task('css', function() {
	return gulp.src('../stylesheets/scss/*.scss')
	.pipe(plumber({ errorHandler: onError }))
	.pipe(sass({outputStyle: 'compressed'}))
	.pipe(gulp.dest('../stylesheets/'))
	.pipe(browsersync.reload({ stream:true }))
	.pipe(notify({ message: 'Styles task complete' }));
});

// Lint JS task
// gulp.task('jslint', function() {
// 	return gulp.src(['../javascripts/class/**/*.js'])
// 	.pipe(jshint())
// 	.pipe(jshint.reporter('default'))
// 	.pipe(jshint.reporter('fail'));
// 	//.pipe(notify({ message: 'Lint task complete' }));
// });

//Concatenate and Minify JS task
gulp.task('scripts', function() {
	return gulp.src(['../javascripts/libraries/jquery.js', '../javascripts/libraries/**/*.js', '../javascripts/class/**/*.js'])
	.pipe(concat('script.js'))
	.pipe(gulp.dest('../javascripts'))
	//.pipe(stripdebug())
	.pipe(uglify())
	.pipe(gulp.dest('../javascripts'))
	.pipe(notify({ message: 'Scripts task complete' }));
});

// Watch task
gulp.task('watch', function () {
	gulp.watch('../stylesheets/scss/**/*', ['css']);
	gulp.watch('../javascripts/class/**/*', ['scripts']);
	gulp.watch(['../**/*.php', '../**/*.html', '../**/*.svg', '../**/*.jpg', '../**/*.png']);
});

//tasks
gulp.task('default', ['css', 'scripts']);
